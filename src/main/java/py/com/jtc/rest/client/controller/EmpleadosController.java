package py.com.jtc.rest.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import py.com.jtc.rest.client.bean.Empleado;
import py.com.jtc.rest.client.service.EmpleadoService;

@Controller
@RequestMapping("/rest-client")
public class EmpleadosController {
	
	@Autowired
	private EmpleadoService empleadoService;

	@GetMapping
	public String index() {
		return "index";
	}
	
	@GetMapping("/empleados")
	public String listaEmpleados(Model model) { 
		model.addAttribute("empleados", empleadoService.obtenerEmpleados());
		return "lista";
	}
	
	@GetMapping("/empleados/add")
	public String empleadoForm(Model model) {
		model.addAttribute("empleado", new Empleado());
		return "form";
	}
	
	@GetMapping("/empleados/edit/{id}")
	public String empleadoEdit(@PathVariable("id") Integer id, Model model) {
		//TODO: Pendiente.. cargar datos del empleado a editar
		return "form";
	}
	
	@PostMapping("/empleados")
	public String agregarEmpleado(@ModelAttribute("empleado") Empleado com) {
		//TODO: Pendiente.. persistir datos
		return "redirect:/rest-client/empleados";
	}
	
	@GetMapping("/empleados/delete/{id}")
	public String deleteEmpleado(@PathVariable("id") int id) {
		empleadoService.eliminarEmpleado(id);
		return "redirect:/rest-client/empleados";
	}

}
